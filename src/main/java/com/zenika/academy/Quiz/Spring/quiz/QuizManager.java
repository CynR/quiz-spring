/*package com.zenika.academy.Quiz.Spring.quiz;

import com.zenika.academy.Quiz.Spring.services.Generator;
import org.springframework.boot.CommandLineRunner;

import org.springframework.stereotype.Component;
import com.zenika.academy.Quiz.Spring.repositories.QuestionRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Component
public class QuizManager implements CommandLineRunner {
    Generator generator;
    Scanner sc;
    QuestionRepository qRepository;

    public QuizManager(Generator generator, Scanner sc, QuestionRepository qRepository){
        this.generator = generator;
        this.sc = sc;
        this.qRepository = qRepository;
    }

    @Override
    public void run(String... args) throws IOException {
       // File f = new File("src/main/resources/tmdb-api-key.txt");
       // TmdbClient tmbclient = new TmdbClient(f);
        //Generator g = Generator.getGeneratorInstance(tmbclient);
        //Scanner sc = new Scanner(System.in);
 //       Player p = createPlayer(sc);
        //list where the questions generated are stocked to prevent ask the user one that has already been asked
        List<String> listExistingQuestions = new ArrayList<>();
        int scoreTotalIfAllAnswersWereCorrect = 0;

        for (int numberRounds = 0; numberRounds < 3; numberRounds++) {
            Question questionDif =
                    this.generator.generate(Generator.getMovieTitle(), Generator.DifficultyLevel.DIFFICILE).get();
            if (!listExistingQuestions.contains(questionDif.getDisplayableText())) {
                //show question on console
 // comment code that uses class Player
 //               p.scorePoints(askQuestion(sc, questionDif));
                //update score
                scoreTotalIfAllAnswersWereCorrect = scoreTotalIfAllAnswersWereCorrect +
                        ((this.generator.generate(Generator.getMovieTitle(), Generator.DifficultyLevel.DIFFICILE).get().getPoints()) * 2);
                //stock question in the list which check it doesn't already exist
                listExistingQuestions.add(questionDif.getDisplayableText());
                //stock question in map
                this.qRepository.saveQuestionFromRepo(questionDif);
            }
            Question questionFac =
                    this.generator.generate(Generator.getMovieTitle(), Generator.DifficultyLevel.FACILE).get();
            this.qRepository.saveQuestionFromRepo(questionFac);
            if (!listExistingQuestions.contains(questionFac.getDisplayableText())) {
 //               p.scorePoints(askQuestion(sc, questionFac));
                scoreTotalIfAllAnswersWereCorrect = scoreTotalIfAllAnswersWereCorrect +
                        ((this.generator.generate(Generator.getMovieTitle(), Generator.DifficultyLevel.FACILE).get().getPoints()) * 2);
                listExistingQuestions.add(questionFac.getDisplayableText());
                this.qRepository.saveQuestionFromRepo(questionFac);
            }
            Question questionMoy =
                    this.generator.generate(Generator.getMovieTitle(), Generator.DifficultyLevel.MOYENNE).get();
            this.qRepository.saveQuestionFromRepo(questionMoy);
            if (!listExistingQuestions.contains(questionMoy.getJustQuestionText())) {
 //               p.scorePoints(askQuestion(sc, questionMoy));
                scoreTotalIfAllAnswersWereCorrect = scoreTotalIfAllAnswersWereCorrect +
                        ((this.generator.generate(Generator.getMovieTitle(), Generator.DifficultyLevel.MOYENNE).get().getPoints()) * 2);
                listExistingQuestions.add(questionMoy.getJustQuestionText());
                this.qRepository.saveQuestionFromRepo(questionMoy);
            }
        }
 //       System.out.println(p.congratulations());
 //       System.out.println("Le score maximum a été de " + scoreTotalIfAllAnswersWereCorrect + " points");
    }


    private static int askQuestion(Scanner sc, Question q) {
        System.out.println(q.getDisplayableText());
        String userAnswer = sc.nextLine();
        switch (q.tryAnswer(userAnswer)) {
            case CORRECT:
                return (2 * q.getPoints());
            case ALMOST_CORRECT:
                return (q.getPoints());
            case INCORRECT:
            default:
                return 0;
        }
    }

    private static Player createPlayer(Scanner sc) {
        System.out.println("Quel est votre nom ?");
        String userName = sc.nextLine();
        return new Player(userName);
    }

}*/
