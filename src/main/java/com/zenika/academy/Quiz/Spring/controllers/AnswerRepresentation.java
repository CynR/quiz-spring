package com.zenika.academy.Quiz.Spring.controllers;

public class AnswerRepresentation {
    private String userAnswer;


    public String getUserAnswer() {
        return this.userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }
}
