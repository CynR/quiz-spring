package com.zenika.academy.Quiz.Spring.quiz;

import org.apache.commons.text.similarity.LevenshteinDistance;

public class OpenQuestion implements Question {

    private final String text;
    private final String correctAnswer;
    private final LevenshteinDistance d;
    private final int points;
    private final long id;

    public OpenQuestion(String text, String correctAnswer, int points, long id) {
        this.text = text;
        this.correctAnswer = correctAnswer;
        this.d = new LevenshteinDistance();
        this.points = points;
        this.id = id;
    }


    /**
     * Returns the text of the question as given in the constructor.
     */
    @Override
    public String getDisplayableText() {
        return this.text;
    }

    /**
     * Try an answer.
     *
     * @param userAnswer the answer as provided by the player.
     * @return CORRECT if the answer is the right one (case insensitive), ALMOST_CORRECT if the levenshtein
     * distance between the given answer and the correct answer is lower than 2, incorrect otherwise.
     */
    @Override
    public AnswerResult tryAnswer(String userAnswer) {
        final Integer distanceWithCorrectAnswer = d.apply(userAnswer.toLowerCase(), correctAnswer.toLowerCase());
        if(distanceWithCorrectAnswer == 0) {
            return AnswerResult.CORRECT;
        }
        else if (distanceWithCorrectAnswer < 2) {
            return AnswerResult.ALMOST_CORRECT;
        }
        else {
            return AnswerResult.INCORRECT;
        }
    }

    public int getPoints()
    {
        return this.points;
    }

    public String getJustQuestionText(){
        return getDisplayableText();
    }

    @Override
    public Long getidQuestion() {
        return this.id;
    }
}
