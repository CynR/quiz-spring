package com.zenika.academy.Quiz.Spring.services;
import com.zenika.academy.Quiz.Spring.quiz.*;
import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

//c'est un factory singleton

@Component
public class Generator {

    public TmdbClient client;
    public AtomicLong generatorId;


    public Generator(TmdbClient client, AtomicLong id){
        this.client = client;
        this.generatorId = id;
    }


    public Optional<Question> generate (String title, DifficultyLevel level){
        Optional<Question> q = Optional.empty();
        switch (level)
        {
            case FACILE:
               q = getMInfo(title).map(this::generateQuestiontrueFalse);
               break;
            case MOYENNE:
               q = getMInfo(title).map(this::generateQuestionMultipleChoice);
               break;
            case DIFFICILE:
               q = getMInfo(title).map(this::generateQuestionOpenQuestions);
               break;
        }
        return q;
    }

    private Optional<MovieInfo> getMInfo (String title){
        return client.getMovie(title);
    }

    //OPEN QUESTION WITH ' FILM'S NAME ' AS CORRECT ANSWER
    private Question generateQuestionOpenQuestions(MovieInfo info) {


        String correctAnswerTitle = info.title;
        int numberOfActors = 3;
        String actors = "";

        if(!info.cast.isEmpty()){
            for(int character = 0; character < numberOfActors; character++){
                actors = actors + info.cast.get(character).actorName + ", ";
            }
            String textWithAnExtraCommaAtTheEnd = "Quel film est sorti dans l'année " + info.year +
                    " qui a comme cast: " + actors;
            int lengthOfThisText = textWithAnExtraCommaAtTheEnd.length();
            String textQuestionAnswerTitle = textWithAnExtraCommaAtTheEnd.substring(0,lengthOfThisText-2) + " ?";
            Question question = new OpenQuestion(textQuestionAnswerTitle,correctAnswerTitle,3, this.generatorId.incrementAndGet());
            return question;
        }
        else
            if (correctAnswerTitle.contains(" ")){
                int indexLastSpaceInMovieName = correctAnswerTitle.lastIndexOf(" ");
                String hintMovieNameIfItContainsMoreThanOneWord =
                        correctAnswerTitle.substring(0,indexLastSpaceInMovieName);
                String textQuestionWithMovieNameIfItContainsMoreThanOneWord =
                        "Quel film est sorti dans l'année " + info.year +
                        " laquel son titre commence avec '" + hintMovieNameIfItContainsMoreThanOneWord + "... ' ?";
                return
                        new OpenQuestion(textQuestionWithMovieNameIfItContainsMoreThanOneWord,
                                correctAnswerTitle,3, generatorId.incrementAndGet());
            }
            int indexOfHalfMovieTitleLength = (correctAnswerTitle.length())/2;
            String hintMovieOneWordName = correctAnswerTitle.substring(0, indexOfHalfMovieTitleLength);
            String textQuestionMovieOneWordName = "Quel film est sorti dans l'année " + info.year +
                    " laquel son titre commence avec '" + hintMovieOneWordName + "... ' ?";
            return new OpenQuestion(textQuestionMovieOneWordName,correctAnswerTitle,3, generatorId.incrementAndGet());
    }

    //MULTIPLE CHOICE QUESTION WITH ' CHARACTERS'S NAME ' AS CORRECT ANSWER IF THERE IS A CAST
    //IF NOT THE YEAR IS THE MULTIPLE CHOICE

    //CHECK NOT ASKING THE SAME QUESTION - BECAUSE LIST OF WRONG ANSWERS DON'T APPEAR IN THE SAME ORDER
    private Question generateQuestionMultipleChoice(MovieInfo info) {
        Random r = new Random();
        String yearString = info.year.toString();
        int yearIncorrect1 = Integer.parseInt(yearString)+10;
        int yearIncorrect2 = Integer.parseInt(yearString)+20;
        int yearIncorrect3 = Integer.parseInt(yearString)-10;

        if(!info.cast.isEmpty()) {
            Question question = new MultipleChoiceQuestion("Dans le film " + info.title + ", " + info.cast.get(1).actorName
                    + " joue le personnage de: ", List.of(info.cast.get(0).characterName, info.cast.get(4).characterName),
                    info.cast.get(1).characterName, r, 2, generatorId.incrementAndGet());

            return question;
        }
        return new MultipleChoiceQuestion("Le film " + info.title + " est sortie dans l'année: ",
                List.of(Integer.toString(yearIncorrect1), Integer.toString(yearIncorrect2),
                        Integer.toString(yearIncorrect3)), info.year.toString(), r, 2, generatorId.incrementAndGet());
    }
    //TRUE-FALSE QUESTION WITH ACTOR'S NAME AS CORRECT ANSWER IF THERE IS A CAST
    //IF NOT THE RELEASE YEAR IS ASKED TO BE CONFIRMED
    private Question generateQuestiontrueFalse(MovieInfo info) {
        Random random = new Random();
        int correctAnswerTrueOrFalse = random.nextInt(2);
        if(!info.cast.isEmpty()) {
            switch (correctAnswerTrueOrFalse){
                case 0:
                    return new TrueFalseQuestion("Le personnage " + info.cast.get(3).characterName
                            + " dans le film " + info.title + " est joué par l'acteur/l'actrice " +
                            info.cast.get(3).actorName, true, 1, generatorId.incrementAndGet());
                case 1:
                    return new TrueFalseQuestion("Le personnage " + info.cast.get(2).characterName
                            + " dans le film " + info.title + " est joué par l'acteur/l'actrice " +
                            info.cast.get(3).actorName, false, 1, generatorId.incrementAndGet());
            }
        }
        return new TrueFalseQuestion("Le film " + info.title + " est sortie dans l'année " + info.year.toString(),
                true, 1, generatorId.incrementAndGet());
    }

    public enum  DifficultyLevel {
        FACILE, MOYENNE, DIFFICILE;
    }
    public static String getMovieTitle(){
        List<ListMovies> listMovies = new ArrayList<>();
        Random r = new Random();

        listMovies.add(new ListMovies("Interstellar"));
        listMovies.add(new ListMovies("Inception"));
        listMovies.add(new ListMovies("Titanic"));
        listMovies.add(new ListMovies("Sherlock Holmes"));
        listMovies.add(new ListMovies("Harry Potter"));
        listMovies.add(new ListMovies("Avatar"));
        listMovies.add(new ListMovies("Seven"));
        listMovies.add(new ListMovies("Forrest Gump"));
        listMovies.add(new ListMovies("Suicide Squad"));
        listMovies.add(new ListMovies("Avengers"));
        listMovies.add(new ListMovies("Hitch"));
        listMovies.add(new ListMovies("Allied"));
        listMovies.add(new ListMovies("The Green Line"));
        listMovies.add(new ListMovies("Burnt"));
        listMovies.add(new ListMovies("No Reservations"));
        listMovies.add(new ListMovies("American Sniper"));
        listMovies.add(new ListMovies("The Lord of the Rings"));
        listMovies.add(new ListMovies("Taken"));
        listMovies.add(new ListMovies("The Notebook"));
        listMovies.add(new ListMovies("The Godfather"));
        listMovies.add(new ListMovies("Armageddon"));
        listMovies.add(new ListMovies("Pearl Harbour"));
        listMovies.add(new ListMovies("Pretty Woman"));
        listMovies.add(new ListMovies("The Hunger Games"));
        listMovies.add(new ListMovies("Shutter Island"));
        int randomNumber = r.nextInt(listMovies.size());
        return listMovies.get(randomNumber).getMovieName();
    }

}
