package com.zenika.academy.Quiz.Spring.repositories;

import com.zenika.academy.Quiz.Spring.quiz.Question;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class QuestionRepository {
    Map<Long, Question> mapQuestionRepository = new HashMap<>();

    public QuestionRepository(){
    }

    public Question getQuestionFromRepo(Long id){
        return this.mapQuestionRepository.get(id);
    }

    public Question deleteQuestionFromRepo(Long id){
        return this.mapQuestionRepository.remove(id);
    }

    public void saveQuestionFromRepo(Question question){
        this.mapQuestionRepository.put(question.getidQuestion(),question);
    }

    public List<Question> getListQuestionsFromRepo(){
        //MAX IS THE BEST OF THE WORLD EVER
        return new ArrayList<>(this.mapQuestionRepository.values());
        //return List.copyOf(this.mapQuestionRepository.values());
        //return this.mapQuestionRepository.values().stream().collect(Collectors.toList());
    }
}
