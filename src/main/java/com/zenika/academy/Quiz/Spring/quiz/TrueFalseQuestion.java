package com.zenika.academy.Quiz.Spring.quiz;

public class TrueFalseQuestion implements Question {

    private String text;
    private boolean isTrue;
    public int points;
    private final long id;

    public TrueFalseQuestion(String text, boolean isTrue, int points, long id) {
        this.text = text;
        this.isTrue = isTrue;
        this.points = points;
        this.id = id;
    }

    /**
     * The text of the question, preceded by the string "VRAI ou FAUX ?"
     */
    @Override
    public String getDisplayableText() {
        return "VRAI ou FAUX ? " + this.text;
    }

    /**
     * Try an answer.
     *
     * If the right answer is true, accepted answers are "true", "oui", "vrai".
     * If the right answer is false, accepted answers are "false", "non", "faux".
     *
     * @param userAnswer the answer as provided by the player.
     * @return CORRECT if the answer is the right one INCORRECT otherwise.
     */
    @Override
    public AnswerResult tryAnswer(String userAnswer) {
        return AnswerResult.fromBoolean(
                (userAnswersYes(userAnswer) && this.isTrue) || userAnswersNo(userAnswer) && !isTrue
        );
    }

    public int getPoints(){
        return this.points;
    }

    private boolean userAnswersNo(String userAnswer) {
        String normalizedAnser = userAnswer.toLowerCase();
        return normalizedAnser.equals("false") || normalizedAnser.equals("non") || normalizedAnser.equals("faux");
    }

    private boolean userAnswersYes(String userAnswer) {
        String normalizedAnser = userAnswer.toLowerCase();
        return normalizedAnser.equals("true") || normalizedAnser.equals("oui") || normalizedAnser.equals("vrai");
    }

    public String getJustQuestionText(){
        return getDisplayableText();
    }

    public Long getidQuestion() {
        return this.id;
    }
}
