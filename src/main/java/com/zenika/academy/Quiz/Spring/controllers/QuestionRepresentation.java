package com.zenika.academy.Quiz.Spring.controllers;

public class QuestionRepresentation {
    private long id;
    private String displayTextQuestion;

    public QuestionRepresentation(long id, String displayTextQuestion) {
        this.id = id;
        this.displayTextQuestion = displayTextQuestion;
    }

    public String getDisplayTextQuestion() {
        return this.displayTextQuestion;
    }

    public void setDisplayTextQuestion(String displayTextQuestion) {
        this.displayTextQuestion = displayTextQuestion;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

