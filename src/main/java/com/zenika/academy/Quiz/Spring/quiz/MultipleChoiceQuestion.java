package com.zenika.academy.Quiz.Spring.quiz;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static java.lang.System.lineSeparator;

public class MultipleChoiceQuestion implements Question {
    private final String text;
    private final List<String> suggestions;
    private final Integer correctAnswerIndex;
    public int points;
    private final long id;

    public MultipleChoiceQuestion(String text, List<String> incorrectSuggestions, String correctAnswer,
                                  Random random, int points, long id) {
        this.text = text;
        this.suggestions = new ArrayList<>(incorrectSuggestions);
        this.suggestions.add(correctAnswer);
        Collections.shuffle(this.suggestions, random);
        correctAnswerIndex = this.suggestions.indexOf(correctAnswer);
        this.points = points;
        this.id = id;
    }

    /**
     * The text of the question, followed by the suggestions in a randomized order. Each suggestion also
     * has an index.
     */
    @Override
    public String getDisplayableText() {
        StringBuilder sb = new StringBuilder(this.text + lineSeparator());
        for (int i = 0; i < suggestions.size(); i++) {
            String suggestion = suggestions.get(i);
            sb.append("  ").append(i+1).append(") ").append(suggestion).append(lineSeparator());
        }
        return sb.toString();
    }

    /**
     * Try an answer.
     *
     * @param userAnswer the answer as provided by the player.
     * @return CORRECT if the answer is the right one (case insensitive), or if the answer is the number
     * of the right one; INCORRECT otherwise.
     */
    @Override
    public AnswerResult tryAnswer(String userAnswer) {
        return AnswerResult.fromBoolean(
                userAnswerIsCorrectIndex(userAnswer) || userAnswerIsCorrectAnswer(userAnswer)
        );
    }

    public int getPoints(){
        return this.points;
    }

    private boolean userAnswerIsCorrectAnswer(String userAnswer) {
        return userAnswer.toLowerCase().equals(suggestions.get(correctAnswerIndex).toLowerCase());
    }

    private boolean userAnswerIsCorrectIndex(String userAnswer) {
        return String.valueOf(this.correctAnswerIndex+1).equals(userAnswer);
    }

    public String getJustQuestionText() {
        return this.text;
    }

    public Long getidQuestion() {
        return this.id;
    }
}
