package com.zenika.academy.Quiz.Spring.controllers;

import com.zenika.academy.Quiz.Spring.services.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/questions")
public class QuestionController {

    private final QuestionService qService;

    @Autowired
    public QuestionController(QuestionService qService) {
        this.qService = qService;
    }

    //shows all questions that are stocked in the repository (HashMap)
    @GetMapping
    public List<QuestionRepresentation> questionList(){
        return this.qService.getAllQuestionsFromRepo();
    }

    //shows the question text and its ID by giving the question ID
    @GetMapping("/{id}")
    public QuestionRepresentation getQueestionById(@PathVariable("id") long id){
        return this.qService.getQuestiontextAndId(id);
    }

    /*let the user create a question by giving a Movie title, in case the user 'makes' a question that already exists
    the question is showed but it is not stocked in the repository (HashMap), if not it's stocked and showed */
    //MAKE DIFFICULTY THE 2ND PARAMETER
    @PostMapping
    public QuestionRepresentation createQuestion(@RequestParam(value = "movie", defaultValue = "Inception") String movie){
        return this.qService.createQuestionFromUser(movie);
    }

    @RequestMapping(value = "/{id}/questions/{userAnswer}", method = RequestMethod.POST)
    public String tryAnserByGivingIdAndAnswerOnURL(@PathVariable("id") long id, @PathVariable("userAnswer") String userAnswer){
        return this.qService.tryUserAnswer(id, userAnswer);
    }

    @PostMapping("/{id}/:tryAnswerFromJson")
    public String tryAnserByGivingIdAndAnswerOnBodyJson(
            @PathVariable("id") long id, @RequestBody AnswerRepresentation userAnswer){
                return this.qService.tryAnswerFromJson(id, userAnswer);
    }

}
