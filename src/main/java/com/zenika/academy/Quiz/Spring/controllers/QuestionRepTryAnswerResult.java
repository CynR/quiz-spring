package com.zenika.academy.Quiz.Spring.controllers;

public class QuestionRepTryAnswerResult {
    private String userAnswer;
    private String result;

    public QuestionRepTryAnswerResult(String userAnswer, String result){
        this.userAnswer = userAnswer;
        this.result = result;
    }

    public String getResult() {
        return this.result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getUserAnswer() {
        return this.userAnswer;
    }

    public void setUserAnswer(String userAnswer){
        this.userAnswer = userAnswer;
    }
}
