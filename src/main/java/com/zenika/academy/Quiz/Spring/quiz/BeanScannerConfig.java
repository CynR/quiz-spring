package com.zenika.academy.Quiz.Spring.quiz;

import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.boot.autoconfigure.web.servlet.ConditionalOnMissingFilterBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Scanner;

@Configuration
public class BeanScannerConfig {

    @Bean
    public Scanner scanner(ApplicationContext context){
        return new Scanner(System.in);
    }
}
