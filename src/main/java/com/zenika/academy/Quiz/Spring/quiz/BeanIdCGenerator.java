package com.zenika.academy.Quiz.Spring.quiz;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanIdCGenerator {

    @Bean
    public AtomicLong generatorId (ApplicationContext ctx){
        return new AtomicLong();
    }


}
