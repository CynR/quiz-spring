package com.zenika.academy.Quiz.Spring.services;

import com.zenika.academy.Quiz.Spring.controllers.AnswerRepresentation;
import com.zenika.academy.Quiz.Spring.controllers.QuestionRepresentation;
import com.zenika.academy.Quiz.Spring.quiz.AnswerResult;
import com.zenika.academy.Quiz.Spring.quiz.Player;
import com.zenika.academy.Quiz.Spring.quiz.Question;
import com.zenika.academy.Quiz.Spring.repositories.QuestionRepository;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Component
public class QuestionService {
    private final Generator generator;
    private final QuestionRepository qRepository;;

    public QuestionService(Generator generator, QuestionRepository qRepository) {
        this.generator = generator;
        this.qRepository = qRepository;
    }

    @PostConstruct
    public void stockQuestions(){
        List<String> listExistingQuestions = new ArrayList<>();
        int scoreTotalIfAllAnswersWereCorrect = 0;

        for (int numberRounds = 0; numberRounds < 10; numberRounds++) {
            Question questionDif =
                    this.generator.generate(Generator.getMovieTitle(), Generator.DifficultyLevel.DIFFICILE).get();
            if (!listExistingQuestions.contains(questionDif.getDisplayableText())) {
                //show question on console
                // comment code that uses class Player
                //               p.scorePoints(askQuestion(sc, questionDif));
                //update score
                scoreTotalIfAllAnswersWereCorrect = scoreTotalIfAllAnswersWereCorrect +
                        ((questionDif.getPoints()) * 2);
                //stock question in the list which check it doesn't already exist
                listExistingQuestions.add(questionDif.getDisplayableText());
                //stock question in map
                this.qRepository.saveQuestionFromRepo(questionDif);
            }
            Question questionFac =
                    this.generator.generate(Generator.getMovieTitle(), Generator.DifficultyLevel.FACILE).get();
            this.qRepository.saveQuestionFromRepo(questionFac);
            if (!listExistingQuestions.contains(questionFac.getDisplayableText())) {
                //               p.scorePoints(askQuestion(sc, questionFac));
                scoreTotalIfAllAnswersWereCorrect = scoreTotalIfAllAnswersWereCorrect +
                        ((questionFac.getPoints()) * 2);
                listExistingQuestions.add(questionFac.getDisplayableText());
                this.qRepository.saveQuestionFromRepo(questionFac);
            }
            Question questionMoy =
                    this.generator.generate(Generator.getMovieTitle(), Generator.DifficultyLevel.MOYENNE).get();
            this.qRepository.saveQuestionFromRepo(questionMoy);
            if (!listExistingQuestions.contains(questionMoy.getJustQuestionText())) {
                //               p.scorePoints(askQuestion(sc, questionMoy));
                scoreTotalIfAllAnswersWereCorrect = scoreTotalIfAllAnswersWereCorrect +
                        ((questionMoy.getPoints()) * 2);
                listExistingQuestions.add(questionMoy.getJustQuestionText());
                this.qRepository.saveQuestionFromRepo(questionMoy);
            }
        }
        //       System.out.println(p.congratulations());
        //       System.out.println("Le score maximum a été de " + scoreTotalIfAllAnswersWereCorrect + " points");
    }

    public List<QuestionRepresentation> getAllQuestionsFromRepo(){
        List<QuestionRepresentation> listQuestionRepresentation = new ArrayList<>();
        List<Question> listAllQuestions = this.qRepository.getListQuestionsFromRepo();
        for(Question q : listAllQuestions) {
            listQuestionRepresentation.add(new QuestionRepresentation(q.getidQuestion(),q.getDisplayableText()));
        }
        return listQuestionRepresentation;
    }

    public QuestionRepresentation getQuestiontextAndId(Long id){
        QuestionRepresentation qRep = new QuestionRepresentation(this.qRepository.getQuestionFromRepo(id).getidQuestion(),
                this.qRepository.getQuestionFromRepo(id).getDisplayableText());
        return qRep;
    }


    public QuestionRepresentation createQuestionFromUser(String movieName){
        Question questionUser = this.generator.generate(movieName, Generator.DifficultyLevel.DIFFICILE).get();
        List<String> listAllTextQuestions = new ArrayList<>();
        List<Question> listAllQuestions = this.qRepository.getListQuestionsFromRepo();
        int counter = 0;
        for (Question q : listAllQuestions){
            listAllTextQuestions.add(q.getJustQuestionText());
        }
        for(String textQuestion : listAllTextQuestions){
            if (!questionUser.getJustQuestionText().equals(textQuestion)){
                counter++;
                if (counter==listAllTextQuestions.size()) {
                    this.qRepository.saveQuestionFromRepo(questionUser);
                    return new QuestionRepresentation(questionUser.getidQuestion(), questionUser.getDisplayableText());
                }
            }
        }
        return new QuestionRepresentation(questionUser.getidQuestion(), questionUser.getDisplayableText());
    }

    public String tryUserAnswer(Long id, String userAnswer){
        Question question = this.qRepository.getQuestionFromRepo(id);
        AnswerResult answerResult = this.qRepository.getQuestionFromRepo(id).tryAnswer(userAnswer);
        int scorePlayer= 0;
        switch (answerResult) {
            case CORRECT:
                scorePlayer = scorePlayer+ (2 * question.getPoints());
                return "CORRECT";
            case ALMOST_CORRECT:
                scorePlayer = scorePlayer+ (question.getPoints());
                return "ALMOST_CORRECT";
            case INCORRECT:
            default:
                return "INCORRECT";
        }
    }

    public String tryAnswerFromJson(Long id, AnswerRepresentation userAnswerRep){
        Question question = this.qRepository.getQuestionFromRepo(id);
        AnswerResult answerResult = this.qRepository.getQuestionFromRepo(id).tryAnswer(userAnswerRep.getUserAnswer());
        int scorePlayer= 0;
        switch (answerResult) {
            case CORRECT:
                scorePlayer = scorePlayer+ (2 * question.getPoints());
                return "CORRECT, BRAVOOOOOOOOOOOOOO";
            case ALMOST_CORRECT:
                scorePlayer = scorePlayer+ (question.getPoints());
                return "ALMOST_CORRECT";
            case INCORRECT:
            default:
                return "INCORRECT, SHAME ON YOU :( ";
        }
    }


    private static int askQuestion(Scanner sc, Question q) {
        System.out.println(q.getDisplayableText());
        String userAnswer = sc.nextLine();
        switch (q.tryAnswer(userAnswer)) {
            case CORRECT:
                return (2 * q.getPoints());
            case ALMOST_CORRECT:
                return (q.getPoints());
            case INCORRECT:
            default:
                return 0;
        }
    }

    private static Player createPlayer(Scanner sc) {
        System.out.println("Quel est votre nom ?");
        String userName = sc.nextLine();
        return new Player(userName);
    }

  /*  public String toString(){
        return
    }*/


}
