package com.zenika.academy.Quiz.Spring.quiz;

import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

@Configuration
public class BeanTmdbClientConfig  {
    @Bean
    public TmdbClient tmdbClient(ApplicationContext context) throws IOException {
        File f = new File("src/main/resources/tmdb-api-key.txt");
        return new TmdbClient(f);
    }
}
