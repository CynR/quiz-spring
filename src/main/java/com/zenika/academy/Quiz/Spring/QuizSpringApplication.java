package com.zenika.academy.Quiz.Spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@SpringBootApplication
public class QuizSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuizSpringApplication.class, args);
	}


}
