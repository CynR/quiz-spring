package com.zenika.academy.Quiz.Spring.quiz;

public class ListMovies {

    String movieName;

    public ListMovies(String movieName) {
        this.movieName = movieName;
    }

    public String getMovieName() {
        return movieName;
    }
}
