package com.zenika.academy.Quiz.Spring.quiz;

import com.zenika.academy.Quiz.Spring.services.Generator;
import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import java.time.Year;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;



@ExtendWith(MockitoExtension.class)
public class GeneratorTest {

    @Mock
    private TmdbClient mocktmbclient;

    @InjectMocks
    private Generator generator;


    @Test
    void doesTheQuestionIsGenerate(){

        //File f = new File("src/main/resources/tmdb-api-key.txt");
        MovieInfo info = new MovieInfo("Interstellar", Year.of(2014), List.of(
                new MovieInfo.Character("Joseph \"Coop\" Cooper", "Matthew McConaughey"),
                new MovieInfo.Character("Murphy \"Murph\" Cooper", "Jessica Chastain"),
                new MovieInfo.Character("Dr. Amelia Brand", "Anne Hathaway"),
                new MovieInfo.Character("Professor John Brand", "Michael Caine"),
                new MovieInfo.Character("Tom Cooper", "Casey Affleck")
        ));
        //TmdbClient mocktmbclient = mock(TmdbClient.class);
        Generator g = this.generator;
        when(this.mocktmbclient.getMovie("Interstellar")).thenReturn(Optional.of(info));

       ///Generator g = Generator.getGeneratorInstance(mocktmbclient);

        int numberOfActors = 3;
        String actors = "";
        for(int character = 0; character < numberOfActors; character++){
            actors = actors + info.cast.get(character).actorName + ", ";
        }
        String tBefore = "Quel film est sorti dans l'année " + info.year + " qui a comme cast: " + actors;
        int len = tBefore.length();
        String textQTitle = tBefore.substring(0,len-2) + " ?";

        Assertions.assertEquals(textQTitle,
                (g.generate("Interstellar", Generator.DifficultyLevel.DIFFICILE).get()).getDisplayableText());
        assertTrue((g.generate("Interstellar", Generator.DifficultyLevel.DIFFICILE)).isPresent());

    }

    @Test
    void doesTheQuestionIsGenerateWithoutGivingAMovieTitle(){

        //TmdbClient mocktmbclient = mock(TmdbClient.class);
        Generator g = this.generator;
        when(this.mocktmbclient.getMovie("Titanic")).thenReturn(Optional.empty());
       // Generator g = Generator.getGeneratorInstance(mocktmbclient);

        Assertions.assertFalse((g.generate("Titanic", Generator.DifficultyLevel.DIFFICILE)).isPresent());
    }



}
