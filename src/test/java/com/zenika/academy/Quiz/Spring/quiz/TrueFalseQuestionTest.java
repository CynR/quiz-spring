package com.zenika.academy.Quiz.Spring.quiz;

import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicLong;

import static com.zenika.academy.Quiz.Spring.quiz.AnswerResult.CORRECT;
import static com.zenika.academy.Quiz.Spring.quiz.AnswerResult.INCORRECT;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertAll;

class TrueFalseQuestionTest {

    private final AtomicLong generatorId = new AtomicLong();

    @Test
    void correctTrueAnswers() {
        Question q = new TrueFalseQuestion("Le mont Saint-Michel est en bretagne", true,1,generatorId.incrementAndGet());

        assertAll(
                () -> assertEquals(CORRECT, q.tryAnswer("oui")),
                () -> assertEquals(CORRECT, q.tryAnswer("vRai")),
                () -> assertEquals(CORRECT, q.tryAnswer("True"))
        );
    }

    @Test
    void correctFalseAnswers() {
        Question q = new TrueFalseQuestion("Le mont Saint-Michel est en normandie", false,1,generatorId.incrementAndGet());

        assertAll(
                () -> assertEquals(CORRECT, q.tryAnswer("non")),
                () -> assertEquals(CORRECT, q.tryAnswer("Faux")),
                () -> assertEquals(CORRECT, q.tryAnswer("FALSE"))
        );
    }

    @Test
    void incorrectTrueAnswers() {
        Question q = new TrueFalseQuestion("La bretagne fait le meilleur cidre", true,1,generatorId.incrementAndGet());

        assertAll(
                () -> assertEquals(INCORRECT, q.tryAnswer("non")),
                () -> assertEquals(INCORRECT, q.tryAnswer("toto")),
                () -> assertEquals(INCORRECT, q.tryAnswer("c'est débattable"))
        );
    }

    @Test
    void incorrectFalseAnswers() {
        Question q = new TrueFalseQuestion("La normandie", false, 1,generatorId.incrementAndGet());

        assertAll(
                () -> assertEquals(INCORRECT, q.tryAnswer("yes")),
                () -> assertEquals(INCORRECT, q.tryAnswer("OK")),
                () -> assertEquals(INCORRECT, q.tryAnswer("true"))
        );
    }
}