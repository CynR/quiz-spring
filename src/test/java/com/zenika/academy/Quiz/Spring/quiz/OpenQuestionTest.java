package com.zenika.academy.Quiz.Spring.quiz;

import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicLong;

import static com.zenika.academy.Quiz.Spring.quiz.AnswerResult.CORRECT;
import static com.zenika.academy.Quiz.Spring.quiz.AnswerResult.INCORRECT;
import static org.junit.jupiter.api.Assertions.*;

class OpenQuestionTest {

    private final AtomicLong generatorId = new AtomicLong();

    @Test
    void getDisplayableText() {
        Question q = new OpenQuestion("Qui marche de travers ?", "C'est le crabe tout vert",3, generatorId.incrementAndGet());

        assertEquals("Qui marche de travers ?", q.getDisplayableText());
    }

    @Test
    void tryCorrectAnswer() {
        Question q = new OpenQuestion("Qui marche de travers ?", "C'est le crabe tout vert",3,generatorId.incrementAndGet());

        assertEquals(CORRECT, q.tryAnswer("C'est le crabe tout vert"));
    }

    @Test
    void tryIncorrectAnswer() {
        Question q = new OpenQuestion("Qui marche de travers ?", "C'est le crabe tout vert",3,generatorId.incrementAndGet());

        assertEquals(INCORRECT, q.tryAnswer("Un homme en état d'ébriété"));
    }

    @Test
    void tryAlmostCorrectAnswer() {
        Question q = new OpenQuestion("Qui marche de travers ?", "C'est le crabe tout vert",3,generatorId.incrementAndGet());

        assertEquals(INCORRECT, q.tryAnswer("C'est le crbe tout vart"));
    }
}